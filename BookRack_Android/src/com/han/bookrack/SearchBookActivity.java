package com.han.bookrack;

import java.util.ArrayList;
import com.han.bookrack.listView.BookItem;
import com.han.bookrack.listView.MyListAdapter;
import com.han.bookrack.listView.MyListItem;
import com.han.bookrack.model.Book;
import com.han.utility.DialogUtility;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

public class SearchBookActivity extends Activity {
	
	public static ArrayList<Book> arrBooks;
	
	ListView lstBook;
	
	CheckBox chkTitle;
	CheckBox chkAuthor;
	CheckBox chkGenre;
	CheckBox chkOwn;
	
	Button btnSearch;
	EditText edtSearch;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	@SuppressLint("NewApi")
	private void initWidget() {
		
	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
		lstBook = (ListView) findViewById(R.id.post_listView);
		
		btnSearch = (Button) findViewById(R.id.book_search_button);
		edtSearch = (EditText) findViewById(R.id.book_search_editText);
		
		chkTitle = (CheckBox) findViewById(R.id.title_checkbox);
		chkAuthor = (CheckBox) findViewById(R.id.author_checkbox);
		chkGenre = (CheckBox) findViewById(R.id.genre_checkbox);
		chkOwn = (CheckBox) findViewById(R.id.own_checkbox);
		
		chkTitle.setChecked(true);
		chkAuthor.setChecked(true);
		chkGenre.setChecked(true);
		chkOwn.setChecked(true);
	}
	
	@SuppressLint("DefaultLocale")
	private void initValue() {
		ArrayList<MyListItem> arrBookItems = new ArrayList<MyListItem>();
		
		String key = edtSearch.getText().toString();
		for (Book book: arrBooks) {
			BookItem bookItem = new BookItem(book, false);
			if (key.equals("") || key == null) {
				arrBookItems.add(bookItem);
				continue;
			}
			if (chkTitle.isChecked()) {
				if (book.title.toLowerCase().indexOf(key.toLowerCase()) >= 0) {
					arrBookItems.add(bookItem);
					continue;
				}
			}
			if (chkAuthor.isChecked()) {
				if (book.author.toLowerCase().indexOf(key.toLowerCase()) >= 0) {
					arrBookItems.add(bookItem);
					continue;
				}
			}
			if (chkGenre.isChecked()) {
				if (book.genre.toLowerCase().indexOf(key.toLowerCase()) >= 0) {
					arrBookItems.add(bookItem);
					continue;
				}
			}
			if (chkOwn.isChecked()) {
				if (book.own.toLowerCase().indexOf(key.toLowerCase()) >= 0) {
					arrBookItems.add(bookItem);
					continue;
				}
			}
		}
		
		if (arrBookItems.size() == 0) {
			DialogUtility.show(this, "Couldn�t find this item");
		}
		
		MyListAdapter adpBooks = new MyListAdapter(this, arrBookItems);
		lstBook.setAdapter(adpBooks);
	}
	
	private void initEvent() {
		btnSearch.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				initValue();
			}
        });
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
