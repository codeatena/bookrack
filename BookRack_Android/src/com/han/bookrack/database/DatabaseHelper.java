package com.han.bookrack.database;

import com.han.bookrack.model.Book;
import com.han.bookrack.model.Post;
import com.han.bookrack.model.Status;
import com.han.bookrack.model.User;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "bookrack.db";
    
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
//		Message.createTable(db);
		
		User.createTable(db);
		Book.createTable(db);
		Status.createTable(db);
		Post.createTable(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
//		Message.dropTable(db);
//		User.dropTable(db);
		
		User.createTable(db);
		Book.createTable(db);
		Status.createTable(db);
		Post.createTable(db);
		
        onCreate(db);
	}
}