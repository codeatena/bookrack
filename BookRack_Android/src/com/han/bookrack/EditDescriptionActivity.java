package com.han.bookrack;

import com.han.bookrack.main.MainActivity;
import com.han.bookrack.model.User;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

public class EditDescriptionActivity extends Activity {

	EditText edtName;
	public static String defaultName = ""; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_name);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	@SuppressLint("NewApi")
	private void initWidget() {
	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	}
	
	private void initValue() {
		edtName = (EditText) findViewById(R.id.name_editText);
		edtName.setText(defaultName);
	}
	
	private void initEvent() {
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.edit_name_activity_action, menu);

	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        case R.id.action_save:
	        	actionSave();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void actionSave() {
		GlobalData.curUser.description = edtName.getText().toString();
		User.updateUser(GlobalData.curUser.id, GlobalData.curUser);
		MainActivity.getInstance().initValue();
		finish();
	}
}
