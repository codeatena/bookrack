package com.han.bookrack.model;

import java.util.ArrayList;

import com.han.bookrack.App;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Post {

	public long id;
	public long user_id;
	public String text;
	
	static final String TABLE_NAME = "tbl_post";
	
	static final String FIELD_ID = "id";
	static final String FIELD_USER_ID = "user_id";
	static final String FIELD_TEXT = "text";
	
	public static void createTable(SQLiteDatabase db) {
		String sqlCreateTable = "CREATE TABLE " + TABLE_NAME + "(" +
				FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				FIELD_USER_ID + " INTEGER, " +
				FIELD_TEXT + " TEXT" + ")";
		db.execSQL(sqlCreateTable);
		
		Log.e("create post table", "success");
	}
	
	public static void dropTable(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		db.close();
	}
	
	public static long addPost(Post post) {
		
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
		
	    ContentValues values = new ContentValues();
	    
	    values.put(FIELD_USER_ID, post.user_id);
	    values.put(FIELD_TEXT, post.text);

	    long record_id = db.insert(TABLE_NAME, null, values);
	    db.close();
	    
	    return record_id;
	}
	
	public static ArrayList<Post> getAllPosts() {
		ArrayList<Post> arrPosts = new ArrayList<Post>();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " ORDER BY id  DESC";
 
        SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        if (cursor.moveToFirst()) {
            do {
            	Post post = new Post();
            	
            	post.id = cursor.getInt(0);
            	post.user_id = cursor.getLong(1);
            	post.text = cursor.getString(2);
            	
            	arrPosts.add(post);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        
        return arrPosts;
	}
	
	public static void deleteRecord(long id) {
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
		
		db.delete(TABLE_NAME, "id=" + Long.toString(id), null);
		db.close();
	}
}