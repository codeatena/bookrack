package com.han.bookrack.model;

import java.util.ArrayList;

import com.han.bookrack.App;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Book {

	public long id;
	public String title = "";
	public String author = "";
	public String genre = "";
	public String own = "";
	public String strImage = "";
	
	static final String TABLE_NAME = "tbl_book";
	
	static final String FIELD_ID = "id";
	static final String FIELD_TITLE = "login_name";
	static final String FIELD_AUTHOR = "full_name";
	static final String FIELD_GENRE = "password";
	static final String FIELD_OWN = "description";
	static final String FIELD_IMAGE = "image";
	
	public static void createTable(SQLiteDatabase db) {
		String sqlCreateTable = "CREATE TABLE " + TABLE_NAME + "(" +
				FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				FIELD_TITLE + " TEXT, " +
				FIELD_AUTHOR + " TEXT, " +
				FIELD_GENRE + " TEXT, " +
				FIELD_OWN + " TEXT, " +
				FIELD_IMAGE + " TEXT" + ")";
		db.execSQL(sqlCreateTable);
		
		Log.e("create book table", "success");
	}
	
	public static void dropTable(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		db.close();
	}
	
	public static long addBook(Book book) {
		
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
		
	    ContentValues values = new ContentValues();
	    
	    values.put(FIELD_TITLE, book.title);
	    values.put(FIELD_AUTHOR, book.author);
	    values.put(FIELD_GENRE, book.genre);
	    values.put(FIELD_OWN, book.own);
	    values.put(FIELD_IMAGE, book.strImage);

	    long record_id = db.insert(TABLE_NAME, null, values);
	    db.close();
	    
	    return record_id;
	}
	
	public static ArrayList<Book> getAllBooks() {
		ArrayList<Book> arrBooks = new ArrayList<Book>();
		String selectQuery = "SELECT * FROM " + TABLE_NAME;
 
        SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        if (cursor.moveToFirst()) {
            do {
            	Book book = new Book();
            	
            	book.id = cursor.getInt(0);
            	book.title = cursor.getString(1);
            	book.author = cursor.getString(2);
            	book.genre = cursor.getString(3);
            	book.own = cursor.getString(4);
            	book.strImage = cursor.getString(5);
            	
            	arrBooks.add(book);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        
        return arrBooks;
	}
	
	public static Book getBook(long id) {
		ArrayList<Book> arrBooks = new ArrayList<Book>();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE" +
			" id='" + Long.toString(id) + "'";
 
        SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        if (cursor.moveToFirst()) {
            do {
            	Book book = new Book();
            	
            	book.id = cursor.getInt(0);
            	book.title = cursor.getString(1);
            	book.author = cursor.getString(2);
            	book.genre = cursor.getString(3);
            	book.own = cursor.getString(4);
            	book.strImage = cursor.getString(5);
            	
            	arrBooks.add(book);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        
        if (arrBooks.size() > 0) {
        	return arrBooks.get(0);
        } else {
        	return null;
        }
	}
	
	public static void updateBook(long id, Book book) {
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
		
	    ContentValues values = new ContentValues();
	    
	    values.put(FIELD_TITLE, book.title);
	    values.put(FIELD_AUTHOR, book.author);
	    values.put(FIELD_GENRE, book.genre);
	    values.put(FIELD_OWN, book.own);
	    values.put(FIELD_IMAGE, book.strImage);
	    
	    db.update(TABLE_NAME, values, "id = ? ", new String[] { Long.toString(id) } );
	    db.close();
	}
}