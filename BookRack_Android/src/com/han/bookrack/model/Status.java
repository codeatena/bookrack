package com.han.bookrack.model;

import java.util.ArrayList;

import com.han.bookrack.App;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Status {

	public static int TO_READ = 0;
	public static int READING = 1;
	public static int READ = 2;
	
	public long id;
	public long user_id;
	public long book_id;
	public int status_read = READING;
	
	static final String TABLE_NAME = "tbl_status";
	
	static final String FIELD_ID = "id";
	static final String FIELD_USER_ID = "user_id";
	static final String FIELD_BOOK_ID = "book_id";
	static final String FIELD_STATUS_READ = "status_read";
	
	public static void createTable(SQLiteDatabase db) {
		String sqlCreateTable = "CREATE TABLE " + TABLE_NAME + "(" +
				FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				FIELD_USER_ID + " INTEGER, " +
				FIELD_BOOK_ID + " INTEGER, " +
				FIELD_STATUS_READ + " INTEGER" + ")";
		db.execSQL(sqlCreateTable);
		
		Log.e("create status table", "success");
	}
	
	public static void dropTable(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		db.close();
	}
	
	public static long addReading(long user_id, long book_id) {
		
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
		
	    ContentValues values = new ContentValues();
	    
	    values.put(FIELD_USER_ID, user_id);
	    values.put(FIELD_BOOK_ID, book_id);
	    values.put(FIELD_STATUS_READ, READING);

	    long record_id = db.insert(TABLE_NAME, null, values);
	    db.close();
	    
	    return record_id;
	}
	
	public static void updateRead(long status_id, long user_id, long book_id) {
		
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
		
	    ContentValues values = new ContentValues();
	    
	    values.put(FIELD_USER_ID, user_id);
	    values.put(FIELD_BOOK_ID, book_id);
	    values.put(FIELD_STATUS_READ, READ);
	    
	    db.update(TABLE_NAME, values, "id = ? ", new String[] { Long.toString(status_id) } );
	    db.close();
	}
	
	public static Status getReading(long user_id) {
		ArrayList<Status> arrRecord = new ArrayList<Status>();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE" +
				" user_id='" + Long.toString(user_id) + "' AND" +
				" status_read='" + Integer.toString(READING) + "'";
 
        SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        if (cursor.moveToFirst()) {
            do {
            	Status status = new Status();
            	
            	status.id = cursor.getInt(0);
            	status.user_id = cursor.getLong(1);
            	status.book_id = cursor.getLong(2);
            	status.status_read = cursor.getInt(3);
            	
            	arrRecord.add(status);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        
        if (arrRecord.size() > 0)
        	return arrRecord.get(0);
        else
        	return null;
	}
	
	public static ArrayList<Status> getRead(long user_id) {
		ArrayList<Status> arrRecord = new ArrayList<Status>();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE" +
				" user_id='" + Long.toString(user_id) + "' AND" +
				" status_read='" + Integer.toString(READ) + "'";
 
        SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        if (cursor.moveToFirst()) {
            do {
            	Status status = new Status();
            	
            	status.id = cursor.getInt(0);
            	status.user_id = cursor.getLong(1);
            	status.book_id = cursor.getLong(2);
            	status.status_read = cursor.getInt(3);
            	
            	arrRecord.add(status);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        
        return arrRecord;
	}
	
	public static ArrayList<Status> getShelves(long user_id) {
		ArrayList<Status> arrRecord = new ArrayList<Status>();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE" +
				" user_id='" + Long.toString(user_id) + "'";
 
        SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        if (cursor.moveToFirst()) {
            do {
            	Status status = new Status();
            	
            	status.id = cursor.getInt(0);
            	status.user_id = cursor.getLong(1);
            	status.book_id = cursor.getLong(2);
            	status.status_read = cursor.getInt(3);
            	
            	arrRecord.add(status);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        
        return arrRecord;
	}
}
