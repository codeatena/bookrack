package com.han.bookrack.model;

import java.util.ArrayList;

import com.han.bookrack.App;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class User {
	
	public static final int EXIST_USER = -1;
	
	public long id;
	public String loginName = "";
	public String fullName = "";
	public String password = "";
	public String description = "I am a student.";
	public String strPhoto = "";
	
	static final String TABLE_NAME = "tbl_user";
	
	static final String FIELD_ID = "id";
	static final String FIELD_LOGIN_NAME = "login_name";
	static final String FIELD_FULL_NAME = "full_name";
	static final String FIELD_PASSWORD = "password";
	static final String FIELD_DESCRIPTION = "description";
	static final String FIELD_PHOTO = "photo";
	
	public static void createTable(SQLiteDatabase db) {
		String sqlCreateTable = "CREATE TABLE " + TABLE_NAME + "(" +
				FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				FIELD_LOGIN_NAME + " TEXT, " +
				FIELD_FULL_NAME + " TEXT, " +
				FIELD_PASSWORD + " TEXT, " +
				FIELD_DESCRIPTION + " TEXT, " +
				FIELD_PHOTO + " TEXT" + ")";
		db.execSQL(sqlCreateTable);
		
		Log.e("create user table", "success");
	}
	
	public static void dropTable(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		db.close();
	}
	
	public static long addUser(User user) {
		
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
		
		User tmpUser = User.getUser(user.loginName);
		if (tmpUser != null) {
			return EXIST_USER;
		}
	    ContentValues values = new ContentValues();
	    
	    values.put(FIELD_LOGIN_NAME, user.loginName);
	    values.put(FIELD_FULL_NAME, user.fullName);
	    values.put(FIELD_PASSWORD, user.password);
	    values.put(FIELD_DESCRIPTION, user.description);
	    values.put(FIELD_PHOTO, user.strPhoto);

	    long record_id = db.insert(TABLE_NAME, null, values);
	    db.close();
	    
	    return record_id;
	}
	
	public static User getUser(String loginName) {
		ArrayList<User> arrRecord = new ArrayList<User>();
		String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE" 
				+ " login_name='" + loginName + "'";
 
        SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        if (cursor.moveToFirst()) {
            do {
            	User user = new User();
            	
            	user.id = cursor.getInt(0);
            	user.loginName = cursor.getString(1);
            	user.fullName = cursor.getString(2);
            	user.password = cursor.getString(3);
            	user.description = cursor.getString(4);
            	user.strPhoto = cursor.getString(5);
            	
                arrRecord.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        
        if (arrRecord.size() > 0)
        	return arrRecord.get(0);
        else
        	return null;
	}
	
	public static User getUserFromID(long userID) {
		ArrayList<User> arrRecord = new ArrayList<User>();
		String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE" 
				+ " id='" + Long.toString(userID) + "'";
 
        SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        if (cursor.moveToFirst()) {
            do {
            	User user = new User();
            	
            	user.id = cursor.getInt(0);
            	user.loginName = cursor.getString(1);
            	user.fullName = cursor.getString(2);
            	user.password = cursor.getString(3);
            	user.description = cursor.getString(4);
            	user.strPhoto = cursor.getString(5);
            	
                arrRecord.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        
        if (arrRecord.size() > 0)
        	return arrRecord.get(0);
        else
        	return null;
	}
	
	public static void updateUser(long id, User user) {
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
		
	    ContentValues values = new ContentValues();
	    
	    values.put(FIELD_LOGIN_NAME, user.loginName);
	    values.put(FIELD_FULL_NAME, user.fullName);
	    values.put(FIELD_PASSWORD, user.password);
	    values.put(FIELD_DESCRIPTION, user.description);
	    values.put(FIELD_PHOTO, user.strPhoto);
	    
	    db.update(TABLE_NAME, values, "id = ? ", new String[] { Long.toString(id) } );
	    db.close();
	}
}