package com.han.bookrack.main;

import com.han.bookrack.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MessageFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_message, container, false);
		
		initValue();
		initEvent();
		
		return rootView;
	}
	
	public void initValue() {
		if (getActivity() == null) return;
	}
	
	public void initEvent() {
		
	}
}