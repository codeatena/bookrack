package com.han.bookrack.main;

import com.han.bookrack.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

public class TabbedFragment extends Fragment implements OnTabChangeListener {
	
	private TabHost mTabHost;
	
	View rootView;
	
	static ViewPager viewPager;
	TabsPagerAdapter mAdapter;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
        rootView = inflater.inflate(R.layout.fragment_tabbed, container, false);
        viewPager = (ViewPager) rootView.findViewById(R.id.pager);
		mAdapter = new TabsPagerAdapter(getFragmentManager());
		
		viewPager.setAdapter(mAdapter);
		mTabHost = (TabHost) rootView.findViewById(android.R.id.tabhost);
		
		setupTabs();
		initEvent();
		
        return rootView;
    }
	
	private void initEvent() {
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {

				mTabHost.setCurrentTab(position);
			}
	
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				
			}
	
			@Override
			public void onPageScrollStateChanged(int arg0) {
				
			}
		});
	}
	
	private void setupTabs() {
		mTabHost.setup();
		
		mTabHost.addTab(newTab("Tab1", R.string.home, R.id.pager));
		mTabHost.addTab(newTab("Tab2", R.string.message, R.id.pager));
		mTabHost.addTab(newTab("Tab3", R.string.profile, R.id.pager));
		mTabHost.addTab(newTab("Tab4", R.string.library, R.id.pager));
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setRetainInstance(true);

		mTabHost.setOnTabChangedListener(this);
		mTabHost.setCurrentTab(1);
		mTabHost.setCurrentTab(0);
	}

	private TabSpec newTab(String tag, int labelID, int tabContentID) {

		TabSpec tabSpec = mTabHost.newTabSpec(tag);
		tabSpec.setIndicator(createTabView(this.getActivity(), tag));
		tabSpec.setContent(tabContentID);
		
		return tabSpec;
	}
	
	@SuppressLint("InflateParams")
	private View createTabView(final Context context, final String text) {
	    View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
	    TextView tv = (TextView) view.findViewById(R.id.tabsText);
	    ImageView ti = (ImageView) view.findViewById(R.id.tab_icon_image);
	    	    
	    if (text.equals("Tab1")) {
	    	ti.setImageResource(R.drawable.home);
		    tv.setText(R.string.home);
	    }
	    if (text.equals("Tab2")) {
	    	ti.setImageResource(R.drawable.messages);
	    	tv.setText(R.string.message);
	    }
	    if (text.equals("Tab3")) {
	    	ti.setImageResource(R.drawable.profile);
	    	tv.setText(R.string.profile);
	    }
	    if (text.equals("Tab4")) {
	    	ti.setImageResource(R.drawable.library);
	    	tv.setText(R.string.library);
	    }
	    
	    return view;
	}

	@Override
	public void onTabChanged(String tabId) {
		if (tabId.equals("Tab1")) {
			updateTab(0);			
			return;
		}
		if (tabId.equals("Tab2")) {
			updateTab(1);
			return;
		}
		if (tabId.equals("Tab3")) {
			updateTab(2);
			return;
		}
		if (tabId.equals("Tab4")) {
			updateTab(3);
			return;
		}
	}

	public static void updateTab(int tabIndex) {
		viewPager.setCurrentItem(tabIndex);
	}
}