package com.han.bookrack.main;

import com.han.bookrack.EditDescriptionActivity;
import com.han.bookrack.EditNameActivity;
import com.han.bookrack.GlobalData;
import com.han.bookrack.R;
import com.han.bookrack.model.User;
import com.han.utility.CameraUtility;
import com.han.utility.ImageUtility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileFragment extends Fragment {

	public ImageView imgMyPhoto;
	public TextView txtFullName;
	public TextView txtDescription;
	
	Button btnLibray;
	private Uri fileUri;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
		
		imgMyPhoto = (ImageView) rootView.findViewById(R.id.my_photo_imageView);
		txtFullName = (TextView) rootView.findViewById(R.id.fullname_textView);
		txtDescription = (TextView) rootView.findViewById(R.id.description_textView);
		btnLibray = (Button) rootView.findViewById(R.id.library_button);
		
		initValue();
		initEvent();
		
		return rootView;
	}
	
	public void initValue() {
	
		if (getActivity() == null) return;
		
		if (GlobalData.curUser == null) {
			return;
		}
		if (GlobalData.curUser.strPhoto != null && !GlobalData.curUser.strPhoto.equals("")) {
			imgMyPhoto.setImageBitmap(ImageUtility.StringToBitmap(GlobalData.curUser.strPhoto));
		}
		
		txtFullName.setText(GlobalData.curUser.fullName);
		txtDescription.setText(GlobalData.curUser.description);
	}
	
	private void initEvent() {
		btnLibray.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				TabbedFragment.updateTab(3);
			}
        });
		txtFullName.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				EditNameActivity.defaultName = GlobalData.curUser.fullName;
				startActivity(new Intent(getActivity(), EditNameActivity.class));
			}
        });
		txtDescription.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				EditDescriptionActivity.defaultName = GlobalData.curUser.description;
				startActivity(new Intent(getActivity(), EditDescriptionActivity.class));
			}
        });
		imgMyPhoto.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
					
					private static final int TAKE_PICTURE = 0;
					private static final int SELECT_PICTURE = 1;
					private static final int CANCEL = 2;
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
							case TAKE_PICTURE:
								Log.e("TAKE_PICTURE", "click");
								takePhoto();
								break;
							case SELECT_PICTURE:
								Log.e("SELECT_PICTURE", "click");
								selectPhoto();
								break;
							case CANCEL:
				    			dialog.dismiss();
				    			break;
						}
					}
				};
				
				new AlertDialog.Builder(getActivity())
			    	.setItems(R.array.dialog_get_picture, listener)
			    	.setCancelable(true)
			    	.show();
			}
        });
	}
	
	public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
 
        fileUri = CameraUtility.getOutputMediaFileUri(CameraUtility.MEDIA_TYPE_IMAGE);
        
        try {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } catch (Exception e) {
        	e.printStackTrace();
        }
 
        startActivityForResult(intent, CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
	
	public void selectPhoto() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, CameraUtility.SELECT_PICTURE_REQUEST_CODE);
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.e("request code", Integer.toString(requestCode));
		if (requestCode == CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) { 	
            	previewImage();
            }
        } else if (requestCode == CameraUtility.SELECT_PICTURE_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				fileUri = data.getData();
				fileUri = ImageUtility.getRealUriFromContentUri(fileUri);
				previewImage();
			}
		}
	}
	
	private void previewImage() {
		Log.e("Image File path", fileUri.getPath());
		
		Bitmap bmp = ImageUtility.getBitmapFromFileUri(fileUri, 50, 50);
		imgMyPhoto.setImageBitmap(bmp);
		
		String strBmp = ImageUtility.BitmapToString(bmp);
		GlobalData.curUser.strPhoto = strBmp;
		
		User.updateUser(GlobalData.curUser.id, GlobalData.curUser);
//		new QBUserService(this).updateMyPhoto(strBmp);
	}
}