package com.han.bookrack.main;

import com.han.bookrack.AddBookActivity;
import com.han.bookrack.LoginActivity;
import com.han.bookrack.R;
import com.han.bookrack.SearchBookActivity;
import com.han.bookrack.model.Book;
import com.han.utility.PreferenceUtility;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends FragmentActivity {
	
	private static MainActivity instance;
	
	public HomeFragment fragHome = null;
	public MessageFragment fragMessage = null;
	public ProfileFragment fragProfile = null;
	public LibraryFragment fragLibrary = null;
	
    public static MainActivity getInstance() {
        return instance;
    }
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		initWidget();
		initValue();
		initEvent();
		
		instance = this;
	}
	
	private void initWidget() {
		
		fragHome = new HomeFragment();
		fragMessage = new MessageFragment();
		fragProfile = new ProfileFragment();
		fragLibrary = new LibraryFragment();
		
		TabbedFragment fragmenttab = new TabbedFragment();
        getSupportFragmentManager().beginTransaction()
        .add(R.id.main_layout, fragmenttab).commit();
	}
	
	public void initValue() {
		
		if (fragHome != null) fragHome.initValue();
		if (fragMessage != null) fragMessage.initValue();
		if (fragProfile != null) fragProfile.initValue();
		if (fragLibrary != null) fragLibrary.initValue();
	}
	
	private void initEvent() {
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main_activity_action, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.action_search:
	            actionSearch();
	            return true;
	        case R.id.add_book:
	        	addBook();
	        	return true;
	        case R.id.logout:
	        	logout();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void actionSearch() {
		SearchBookActivity.arrBooks = Book.getAllBooks();
		startActivity(new Intent(this, SearchBookActivity.class));
	}
	
	private void addBook() {
		startActivity(new Intent(this, AddBookActivity.class));
	}
	
	private void logout() {
		PreferenceUtility.clearCurrentUser();
		startActivity(new Intent(this, LoginActivity.class));
		finish();
	}
}