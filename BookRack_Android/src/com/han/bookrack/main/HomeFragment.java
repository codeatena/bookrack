package com.han.bookrack.main;

import java.util.ArrayList;

import com.han.bookrack.GlobalData;
import com.han.bookrack.R;
import com.han.bookrack.listView.MyListAdapter;
import com.han.bookrack.listView.MyListItem;
import com.han.bookrack.listView.PostItem;
import com.han.bookrack.model.Post;
import com.han.utility.DialogUtility;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class HomeFragment extends Fragment {

	MyListAdapter adpPost;
	
	ListView lstPost;
	EditText edtUpload;
	Button btnUpload;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_home, container, false);
		
		lstPost = (ListView) rootView.findViewById(R.id.post_listView);
		edtUpload = (EditText) rootView.findViewById(R.id.search_editText);
		btnUpload = (Button) rootView.findViewById(R.id.upload_button);
		
		initValue();
		initEvent();
		
		return rootView;
	}
	
	public void initValue() {
		
		if (getActivity() == null) return;
		ArrayList<Post> arrPosts = Post.getAllPosts();
		ArrayList<MyListItem> arrData = new ArrayList<MyListItem>();
		for (Post post: arrPosts) {
			PostItem posItem = new PostItem(post);
			arrData.add(posItem);
		}
		
		adpPost = new MyListAdapter(getActivity(), arrData);
		lstPost.setAdapter(adpPost);
	}
	
	private void initEvent() {
		btnUpload.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				String strUpload = edtUpload.getText().toString();
				if (strUpload == null || strUpload.equals("")) {
					DialogUtility.show(getActivity(), "Empty staus");
					return;
				}
				
				edtUpload.setText("");
				
				Post post = new Post();
				post.user_id = GlobalData.curUser.id;
				post.text = strUpload;
				Post.addPost(post);
				
				MainActivity.getInstance().initValue();
			}
        });
		
		lstPost.setOnItemLongClickListener (new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				PostItem postItem = (PostItem) adpPost.getItem(arg2);
				
				if (postItem.myPost.user_id != GlobalData.curUser.id) {
					DialogUtility.show(getActivity(), "This is not your status!");
				} else {
					Post.deleteRecord(postItem.myPost.id);
					initValue();
				}
				return false;
			}
	    });
	}
}