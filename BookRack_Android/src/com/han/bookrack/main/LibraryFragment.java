package com.han.bookrack.main;

import java.util.ArrayList;

import com.han.bookrack.GlobalData;
import com.han.bookrack.R;
import com.han.bookrack.SearchBookActivity;
import com.han.bookrack.model.Book;
import com.han.bookrack.model.Status;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class LibraryFragment extends Fragment {

	Button btnSearch;
	
	Button btnReading;
	Button btnRead;
	Button btnToRead;
	
	EditText edtSearch;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_library, container, false);
		
		edtSearch = (EditText) rootView.findViewById(R.id.search_editText);
		btnSearch = (Button) rootView.findViewById(R.id.library_book_search_button);
		
		btnReading = (Button) rootView.findViewById(R.id.upload_button);
		btnRead = (Button) rootView.findViewById(R.id.library_button);
		btnToRead = (Button) rootView.findViewById(R.id.to_read_button);
		
		initValue();
		initEvent();
		
		return rootView;
	}
	
	public void initValue() {
		
		if (getActivity() == null) return;
		
		Status status = Status.getReading(GlobalData.curUser.id);
		int nReading = 0;
		if (status != null) {
			nReading = 1;
		}
		
		btnReading.setText("Currently reading (" + Integer.toString(nReading) + ")");
		ArrayList<Status> arrRead = Status.getRead(GlobalData.curUser.id);
		btnRead.setText("Read (" + Integer.toString(arrRead.size()) + ")");
		btnToRead.setText("To-read (0)");
	}
	
	private void initEvent() {
		btnReading.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				SearchBookActivity.arrBooks = new ArrayList<Book>();
				Status status = Status.getReading(GlobalData.curUser.id);
				if (status != null) {
					SearchBookActivity.arrBooks.add(Book.getBook(status.book_id));
				}
				startActivity(new Intent(getActivity(), SearchBookActivity.class));
			}
        });
		btnRead.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				SearchBookActivity.arrBooks = new ArrayList<Book>();
				ArrayList<Status> arrStatus = Status.getRead(GlobalData.curUser.id);
				
				for (Status status: arrStatus) {
					SearchBookActivity.arrBooks.add(Book.getBook(status.book_id));
				}
				
				startActivity(new Intent(getActivity(), SearchBookActivity.class));
			}
        });
		btnToRead.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				SearchBookActivity.arrBooks = new ArrayList<Book>();
				startActivity(new Intent(getActivity(), SearchBookActivity.class));
			}
        });
		btnSearch.setOnClickListener(new Button.OnClickListener() {
			@SuppressLint("DefaultLocale")
			@Override
			public void onClick(View v) {
				SearchBookActivity.arrBooks = new ArrayList<Book>();
				ArrayList<Book> arrBooks = new ArrayList<Book>();
				
				String key = edtSearch.getText().toString();
				
				ArrayList<Status> arrStatus = Status.getShelves(GlobalData.curUser.id);
				for (Status status: arrStatus) {
					arrBooks.add(Book.getBook(status.book_id));
				}
				if (key.equals("") || key == null) {
					SearchBookActivity.arrBooks = arrBooks;
				} else {
					for (Book book: arrBooks) {
						if (book.title.toLowerCase().indexOf(key.toLowerCase()) >= 0) {
							SearchBookActivity.arrBooks.add(book);
						}
					}	
				}
				
				startActivity(new Intent(getActivity(), SearchBookActivity.class));
			}
        });
	}
}