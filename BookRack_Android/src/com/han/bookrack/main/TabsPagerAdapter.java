package com.han.bookrack.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

public class TabsPagerAdapter extends FragmentPagerAdapter {

	final int COUNT_TAB = 4;

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		Log.e("pager index", Integer.toString(index));

		switch (index) {
			case 0:
				return MainActivity.getInstance().fragHome;
			case 1:
				return MainActivity.getInstance().fragMessage;
			case 2:
				return MainActivity.getInstance().fragProfile;
			case 3:
				return MainActivity.getInstance().fragLibrary;
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return COUNT_TAB;
	}
}
