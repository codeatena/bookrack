package com.han.bookrack.listView;

import com.han.bookrack.R;
import com.han.bookrack.model.Post;

public class PostItem extends MyListItem {

	public Post myPost = new Post();
	
	public PostItem(Post post) {
		layoutID = R.layout.row_post;
		myPost = post;
	}
}
