package com.han.bookrack.listView;

import java.util.ArrayList;

import com.han.bookrack.GlobalData;
import com.han.bookrack.R;
import com.han.bookrack.main.MainActivity;
import com.han.bookrack.model.Book;
import com.han.bookrack.model.Post;
import com.han.bookrack.model.Status;
import com.han.bookrack.model.User;
import com.han.utility.ImageUtility;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MyListAdapter extends ArrayAdapter<MyListItem> {
	
	public int nSelectedItem;
	
    public MyListAdapter(Context context, ArrayList<MyListItem> listItems) {
        super(context, 0, listItems);
        nSelectedItem = -1;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	View view = convertView;
    	
    	MyListItem item = getItem(position);
		LayoutInflater inflater = LayoutInflater.from(getContext());

    	if (item instanceof SectionItem) {
    		SectionItem sectionItem = (SectionItem) item;
    		
    		view = inflater.inflate(sectionItem.layoutID, parent, false);
    		view.setOnClickListener(null);
    		view.setOnLongClickListener(null);
    		view.setLongClickable(false);
    		
    		if (sectionItem.layoutID == R.layout.row_section) {
        		TextView txtTitle = (TextView) view.findViewById(R.id.title_textView);
        		txtTitle.setText(sectionItem.title);
    		}
    	}
    	if (item instanceof BookItem) {
    		final BookItem bookItem = (BookItem) item;
    		
    		view = inflater.inflate(item.layoutID, parent, false);
    		ImageView imgBook = (ImageView) view.findViewById(R.id.row_book_imageView);
    		
    		TextView txtTitle = (TextView) view.findViewById(R.id.book_title_textView);
    		TextView txtAuthor = (TextView) view.findViewById(R.id.book_author_textView);
    		TextView txtGenre = (TextView) view.findViewById(R.id.book_genre_textView);
    		TextView txtOwn = (TextView) view.findViewById(R.id.book_own_textView);
    		
    		Button btnAdd = (Button) view.findViewById(R.id.add_button);
    		
    		imgBook.setImageBitmap(ImageUtility.StringToBitmap(bookItem.myBook.strImage));
    		txtTitle.setText(bookItem.myBook.title);
    		txtAuthor.setText("Author: " + bookItem.myBook.author);
    		txtGenre.setText("Genre: " + bookItem.myBook.genre);
    		txtOwn.setText("Owned by " + bookItem.myBook.own);
    		
    		if (bookItem.isAdded) {
    			btnAdd.setVisibility(View.GONE);
    		} else {
    			btnAdd.setVisibility(View.VISIBLE);
    		}
    		
    		btnAdd.setOnClickListener(new Button.OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				bookItem.isAdded = true;
    				Status status = Status.getReading(GlobalData.curUser.id);
    				Post post = null;
    				if (status != null) {
    					Status.updateRead(status.id, GlobalData.curUser.id, status.book_id);
    					post = new Post();
    					post.user_id = GlobalData.curUser.id;
    					Book book = Book.getBook(status.book_id);
    					if (book != null) {
    						post.text = GlobalData.curUser.fullName + " read " + book.title;
    						Post.addPost(post);
    					}
    				}
    				
    				Status.addReading(GlobalData.curUser.id, bookItem.myBook.id);

    				post = new Post();
    				post.user_id = GlobalData.curUser.id;
    				post.text = GlobalData.curUser.fullName + " is reading " + bookItem.myBook.title;
    				Post.addPost(post);
    				
    				MainActivity.getInstance().initValue();
    			}
            });
    	}
    	if (item instanceof PostItem) {
    		PostItem postItem = (PostItem) item;
    		view = inflater.inflate(item.layoutID, parent, false);
    		
    		ImageView imgPhoto = (ImageView) view.findViewById(R.id.user_photo_imageView);
    		TextView txtPost = (TextView) view.findViewById(R.id.post_textView);
    		
    		long user_id = postItem.myPost.user_id;
    		User user = User.getUserFromID(user_id);
    		
    		if (user != null) {
    			if (user.strPhoto != null && !user.strPhoto.equals("")) {
        			imgPhoto.setImageBitmap(ImageUtility.StringToBitmap(user.strPhoto));	
        		}	
    		}
    		
    		txtPost.setText(postItem.myPost.text);
    	}
    	
        return view;
    }
    
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
		  // pre-condition
        	return;
		}

	    int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	    	View listItem = listAdapter.getView(i, null, listView);
	    	
	    	if (listItem instanceof ViewGroup) {
	    		listItem.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	    	}
	    	
	    	listItem.measure(0, 0);
	    	totalHeight += listItem.getMeasuredHeight();
	    }

	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
	    
	    listView.setLayoutParams(params);
	    listView.requestLayout();
    }
}