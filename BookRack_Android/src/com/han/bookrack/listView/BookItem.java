package com.han.bookrack.listView;

import com.han.bookrack.R;
import com.han.bookrack.model.Book;

public class BookItem extends MyListItem {

	Book myBook = new Book();
	boolean isAdded = false;
	
	public BookItem(Book book, boolean _isAdded) {
		layoutID = R.layout.row_search_book;
		myBook = book;
		isAdded = _isAdded;
	}
}
