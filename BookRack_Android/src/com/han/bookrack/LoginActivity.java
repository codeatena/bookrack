package com.han.bookrack;

import com.han.bookrack.main.MainActivity;
import com.han.bookrack.model.User;
import com.han.utility.DialogUtility;
import com.han.utility.PreferenceUtility;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends Activity {

	EditText edtUsername;
	EditText edtPassword;
	Button btnLogin;
	Button btnSignup;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		edtUsername = (EditText) findViewById(R.id.username_editText);
		edtPassword = (EditText) findViewById(R.id.password_editText);
		btnLogin = (Button) findViewById(R.id.login_button);
		btnSignup = (Button) findViewById(R.id.signup_button);
	}
	
	private void initValue() {

	}
	
	private void initEvent() {
		btnLogin.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isCheckInput()) {
					actionLogin();
				}
			}
        });
		btnSignup.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(LoginActivity.this, SignupActivity.class));
			}
        });
	}
	
	private boolean isCheckInput() {
		
		String username = edtUsername.getText().toString();
		String password = edtPassword.getText().toString();
		if (username == null || username.equals("")) {
			DialogUtility.showGeneralAlert(this, "Error", "Please input your username.");
			return false;
		}
		if (password == null || password.equals("")) {
			DialogUtility.showGeneralAlert(this, "Error", "Please input your password.");
			return false;
		}
		
		return true;
	}
	
	private void actionLogin() {
		String loginName = edtUsername.getText().toString();
		String password = edtPassword.getText().toString();
		
		GlobalData.curUser = User.getUser(loginName);
		if (GlobalData.curUser != null) {
			if (GlobalData.curUser.password.equals(password)) {
				successLogin();
			} else {
				DialogUtility.showGeneralAlert(this, "Error", "Your password is invalid.");
			}
		} else {
			DialogUtility.show(this, "This user does not exit.");
		}
	}
	
	public void successLogin() {
		PreferenceUtility.setCurrentUser();
		startActivity(new Intent(this, MainActivity.class));
		finish();
	}
}