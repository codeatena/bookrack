package com.han.bookrack;

import com.han.bookrack.database.DatabaseHelper;
import com.han.utility.PreferenceUtility;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class App extends Application {

    private static App instance;
    
    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initApplication();
    }

    private void initApplication() {
        instance = this;
        PreferenceUtility.init(this);
    }
    
    public static DatabaseHelper getDBHelper() {
    	return new DatabaseHelper(instance);
    }
    
    public int getAppVersion() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}