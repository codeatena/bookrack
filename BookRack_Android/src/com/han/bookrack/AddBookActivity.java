package com.han.bookrack;

import com.han.bookrack.model.Book;
import com.han.utility.CameraUtility;
import com.han.utility.DialogUtility;
import com.han.utility.ImageUtility;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class AddBookActivity extends Activity {

	ImageView imgBook;
	
	EditText edtTitle;
	EditText edtAuthor;
	EditText edtGenre;
	EditText edtOwn;
	
	private Uri fileUri;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_book);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	@SuppressLint("NewApi")
	private void initWidget() {
	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    imgBook = (ImageView) findViewById(R.id.book_imageView);
	    edtTitle = (EditText) findViewById(R.id.search_editText);
	    edtAuthor = (EditText) findViewById(R.id.author_editText);
	    edtGenre = (EditText) findViewById(R.id.genre_editText);
	    edtOwn = (EditText) findViewById(R.id.own_editText);
	}
	
	private void initValue() {
		
	}
	
	private void initEvent() {
		imgBook.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
					
					private static final int TAKE_PICTURE = 0;
					private static final int SELECT_PICTURE = 1;
					private static final int CANCEL = 2;
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
							case TAKE_PICTURE:
								Log.e("TAKE_PICTURE", "click");
								takePhoto();
								break;
							case SELECT_PICTURE:
								Log.e("SELECT_PICTURE", "click");
								selectPhoto();
								break;
							case CANCEL:
				    			dialog.dismiss();
				    			break;
						}
					}
				};
				
				new AlertDialog.Builder(AddBookActivity.this)
			    	.setItems(R.array.dialog_get_picture, listener)
			    	.setCancelable(true)
			    	.show();
			}
        });
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturedIntent) {
		if (requestCode == CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) { 	
            	previewImage();
            }
        }
		if (requestCode == CameraUtility.SELECT_PICTURE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				fileUri = imageReturedIntent.getData();
				fileUri = ImageUtility.getRealUriFromContentUri(fileUri);
				previewImage();
			}
		}
	}
	
	private void previewImage() {
		Log.e("Image File path", fileUri.getPath());
				
		Bitmap bmp = ImageUtility.getBitmapFromFileUri(fileUri, 70, 100);
		imgBook.setImageBitmap(bmp);
    }
	
	public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
 
        fileUri = CameraUtility.getOutputMediaFileUri(CameraUtility.MEDIA_TYPE_IMAGE);
        
        try {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } catch (Exception e) {
        	e.printStackTrace();
        }
 
        startActivityForResult(intent, CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
	
	public void selectPhoto() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, CameraUtility.SELECT_PICTURE_REQUEST_CODE);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.edit_name_activity_action, menu);

	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        case R.id.action_save:
	        	actionSaveBook();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void actionSaveBook() {		
		if (isCheckInput()) {
			Book book = new Book();
			book.title = edtTitle.getText().toString();
			book.author = edtAuthor.getText().toString();
			book.genre = edtGenre.getText().toString();
			book.own = edtOwn.getText().toString();
			book.strImage = ImageUtility.BitmapToString(ImageUtility.getBitmapFromImageView(imgBook));
			Book.addBook(book);
			
			finish();
		}
	}
	
	private boolean isCheckInput() {
		String title = edtTitle.getText().toString();
		String author = edtAuthor.getText().toString();
		String genre = edtGenre.getText().toString();
		String own = edtOwn.getText().toString();
		
		if (title == null || title.equals("")) {
			DialogUtility.showGeneralAlert(this, "Error", "Please input title");
			return false;
		}
		if (author == null || author.equals("")) {
			DialogUtility.showGeneralAlert(this, "Error", "Please input author");
			return false;
		}
		if (genre == null || genre.equals("")) {
			DialogUtility.showGeneralAlert(this, "Error", "Please input genre");
			return false;
		}
		if (own == null || own.equals("")) {
			DialogUtility.showGeneralAlert(this, "Error", "Please input own");
			return false;
		}
		
		return true;
	}
}
