package com.han.bookrack;

import com.han.bookrack.model.User;
import com.han.utility.DialogUtility;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SignupActivity extends Activity {

	EditText edtUsername;
	EditText edtPassword;
	EditText edtConfirmPassword;

	Button btnSignup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		edtUsername = (EditText) findViewById(R.id.username_editText);
		edtPassword = (EditText) findViewById(R.id.password_editText);
		edtConfirmPassword = (EditText) findViewById(R.id.confirm_password_editText);

		btnSignup = (Button) findViewById(R.id.signup_button);
	}
	
	private void initValue() {
		btnSignup.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isCheckInput()) {
					actionSignup();
				}
			}
        });
	}
	
	private void initEvent() {
		
	}
	
	private boolean isCheckInput() {
		
		String username = edtUsername.getText().toString();
		String password = edtPassword.getText().toString();
		String confirmPassword = edtConfirmPassword.getText().toString();

		if (username == null || username.equals("")) {
			DialogUtility.showGeneralAlert(this, "Error", "Please input your username.");
			return false;
		}
		if (password == null || password.equals("")) {
			DialogUtility.showGeneralAlert(this, "Error", "Please input your password.");
			return false;
		}
		if (confirmPassword == null || confirmPassword.equals("")) {
			DialogUtility.showGeneralAlert(this, "Error", "Please input confirm password.");
			return false;
		}
		if (!password.equals(confirmPassword)) {
			DialogUtility.showGeneralAlert(this, "Error", "Your passwords are not matched.");
			return false;
		}
		
		return true;
	}
	
	private void actionSignup() {
		User user = new User();
		user.loginName = edtUsername.getText().toString();
		user.fullName = edtUsername.getText().toString();
		user.password = edtPassword.getText().toString();
		if (User.addUser(user) != User.EXIST_USER) {
			DialogUtility.show(this, "Signup succesful");
			finish();	
		} else {
			DialogUtility.showGeneralAlert(this, "Error", "This user exist aleady");
		}
	}
}