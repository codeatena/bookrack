package com.han.bookrack;

import com.han.bookrack.main.MainActivity;
import com.han.bookrack.model.User;
import com.han.utility.DialogUtility;
import com.han.utility.PreferenceUtility;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		initWidget();
		initValue();
		initEvent();
		
		if (PreferenceUtility.isLoginAleady()) {
			autoLogin();
		} else {
			startActivity(new Intent(this, LoginActivity.class));
			finish();
		}
	}
	
	private void initWidget() {

	}
	
	private void initValue() {
		
	}
	
	private void initEvent() {

	}
	
	private void autoLogin() {
		
		String loginName = PreferenceUtility.getCurUserLoginName();
		String password = PreferenceUtility.getCurUserPassword();
		
		GlobalData.curUser = User.getUser(loginName);
		if (GlobalData.curUser != null) {
			if (GlobalData.curUser.password.equals(password)) {
				successLogin();	
			} else {
				DialogUtility.showGeneralAlert(this, "Error", "Your password is invalid.");
			}
		} else {
			DialogUtility.showGeneralAlert(this, "Error", "This user does not exit.");
		}
	}
	
	private void successLogin() {
		startActivity(new Intent(this, MainActivity.class));
		finish();
	}
}
