package com.han.utility;

import com.han.bookrack.GlobalData;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferenceUtility {
	
	public static final String PREF_NAME = "bookrack_pref";
	
	public static final String CURRENT_USER_LOGIN = "current_user_login";
	public static final String CURRENT_USER_PASSWORD = "current_user_password";
			
	public static SharedPreferences sharedPreferences;
	public static Editor prefEditor;
	
	public static void init(Context context) {
		if (context != null) {
			sharedPreferences = context.getSharedPreferences(PreferenceUtility.PREF_NAME, Context.MODE_PRIVATE);
	        prefEditor = PreferenceUtility.sharedPreferences.edit();	
		}
	}

	public static String getCurUserLoginName() {
		return sharedPreferences.getString(CURRENT_USER_LOGIN, "");
	}
	
	public static String getCurUserPassword() {
		return sharedPreferences.getString(CURRENT_USER_PASSWORD, "");
	}
	
	public static boolean isLoginAleady() {
		String loginName = PreferenceUtility.getCurUserLoginName();
		if (loginName.equals("")) {
			return false;
		}
		
		return true;
	}
	
	public static void setCurrentUser() {
		if (GlobalData.curUser != null) {
			prefEditor.putString(CURRENT_USER_LOGIN, GlobalData.curUser.loginName);
			prefEditor.putString(CURRENT_USER_PASSWORD, GlobalData.curUser.password);
			prefEditor.commit();
		}
	}
	
	public static void clearCurrentUser() {
		if (GlobalData.curUser != null) {
			prefEditor.putString(CURRENT_USER_LOGIN, "");
			prefEditor.putString(CURRENT_USER_PASSWORD, "");
			prefEditor.commit();
			GlobalData.curUser = null;
		}
	}
}