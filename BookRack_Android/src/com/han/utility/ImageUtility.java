package com.han.utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.han.bookrack.App;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

public class ImageUtility {
	
	public static Bitmap getBitmapFromUrl(String src) {
		try {
	        Log.e("src",src);
	        URL url = new URL(src);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        Bitmap myBitmap = BitmapFactory.decodeStream(input);
	        Log.e("Bitmap","returned");
	        return myBitmap;
	    } catch (IOException e) {
	        e.printStackTrace();
	        Log.e("Exception",e.getMessage());
	        return null;
	    }
	}
	
	public static Bitmap getRotatedBitmap(Bitmap bmpSrc, int orientation) {

		Matrix matrix = new Matrix();

        switch (orientation) {
        	case ExifInterface.ORIENTATION_ROTATE_90:
        		matrix.postRotate(90);
        		break;
        	case ExifInterface.ORIENTATION_ROTATE_180:
        		matrix.postRotate(180);
        		break;
        	case ExifInterface.ORIENTATION_ROTATE_270:
        		matrix.postRotate(270);
        		break;
        }
        
        Bitmap newBmp = Bitmap.createBitmap(bmpSrc, 0, 0,
        		bmpSrc.getWidth(), bmpSrc.getHeight(),
                matrix, true);
        
        return newBmp;
	}
	
	public static Bitmap getBitmapFromFileUri(Uri fileUri, int width, int height) {
		
		if (fileUri == null) return null;
		
		try {
			Bitmap takenBmp = decodeFile(fileUri.getPath());
			
			if (width == 0 && height == 0) {
				takenBmp = Bitmap.createScaledBitmap(takenBmp, takenBmp.getWidth(), takenBmp.getHeight(), true);
			} else {
				takenBmp = Bitmap.createScaledBitmap(takenBmp, width, height, true);	
			}

			ExifInterface ei;
	        ei = new ExifInterface(fileUri.getPath());
	        
	        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
	                ExifInterface.ORIENTATION_NORMAL);
	        
    		takenBmp = getRotatedBitmap(takenBmp, orientation);
    		
    		return takenBmp;
	    } catch (IOException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
	
	public static Bitmap getBitmapFromImageView(ImageView imgView) {
		
		Bitmap bitmap;
		
		Log.e("imageView width, height", Integer.toString(imgView.getWidth()) + ", " + Integer.toString(imgView.getHeight()));
		
		if (imgView.getDrawable() instanceof BitmapDrawable) {
		    bitmap = ((BitmapDrawable) imgView.getDrawable()).getBitmap();
		    bitmap = Bitmap.createScaledBitmap(bitmap, 50, 50, true);
		} else {
		    Drawable d = imgView.getDrawable();
		    bitmap = Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888);
		    Canvas canvas = new Canvas(bitmap);
		    d.draw(canvas);
		}
		
		return bitmap;
	}
	
	public static Bitmap getRoundCornerBmp(Bitmap src, float round) {
		  // Source image size
		  int width = src.getWidth();
		  int height = src.getHeight();
		  // create result bitmap output
		  Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		  // set canvas for painting
		  Canvas canvas = new Canvas(result);
		  canvas.drawARGB(0, 0, 0, 0);
		 
		  // configure paint
		  final Paint paint = new Paint();
		  paint.setAntiAlias(true);
		  paint.setColor(Color.BLACK);
		 
		  // configure rectangle for embedding
		  final Rect rect = new Rect(0, 0, width, height);
		  final RectF rectF = new RectF(rect);
		 
		  // draw Round rectangle to canvas
		  canvas.drawRoundRect(rectF, round, round, paint);
		 
		  // create Xfer mode
		  paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		  // draw source image to canvas
		  canvas.drawBitmap(src, rect, rect, paint);
		 
		  // return final image
		  return result;
	}
	
	@SuppressLint("NewApi")
	public static String BitmapToString(Bitmap bitmap){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte [] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
	}

	@SuppressLint("NewApi")
	public static Bitmap StringToBitmap(String encodedString){
		try{
			byte [] encodeByte = Base64.decode(encodedString,Base64.DEFAULT);
			Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
			return bitmap;
		} catch(Exception e) {
			e.getMessage();
			return null;
		}
	}
	
	public static String getRealPathFromContentUri(Uri contentUri) {
	    String result;
	    Cursor cursor = App.getInstance().getContentResolver().query(contentUri, null, null, null, null);
	    if (cursor == null) { // Source is Dropbox or other similar local file path
	        result = contentUri.getPath();
	    } else { 
	        cursor.moveToFirst(); 
	        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
	        result = cursor.getString(idx);
	        cursor.close();
	    }
	    
	    Log.e("real path", result);
	    return result;
	}
	
	public static Uri getRealUriFromContentUri(Uri contentUri) {
		String realPath = getRealPathFromContentUri(contentUri);
		Uri realUri = Uri.fromFile(new File(realPath));
		
		return realUri;
	}
	
	@SuppressLint("NewApi")
	public static void drawImageUri(ImageView imgView, Uri imgUri) {
		try {
			imgView.setImageBitmap(decodeFile(imgUri.getPath()));
//			imgView.setImageURI(imgUri.getPath());
			ExifInterface ei;
	        ei = new ExifInterface(imgUri.getPath());
	        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
	                ExifInterface.ORIENTATION_NORMAL);
	        switch (orientation) {
	        	case ExifInterface.ORIENTATION_ROTATE_90:
	        		imgView.setRotation(90);
	        		break;
	        	case ExifInterface.ORIENTATION_ROTATE_180:
	        		imgView.setRotation(180);
	        		break;
	        	case ExifInterface.ORIENTATION_ROTATE_270:
	        		imgView.setRotation(270);
	        		break;
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Bitmap decodeFile(String filePath){
        try {
        	File f = new File(filePath);
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_SIZE = 320;

            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        	e.printStackTrace();
        }
        
        return null;
    }
}